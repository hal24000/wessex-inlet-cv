import pandas as pd
import streamlit as st
from PIL import Image
from streamlit_cropper import st_cropper

from model.plot import plot_convolution, plot_cropped
from model.utils import all_sites, get_data, name_to_id
from setup.database import db_con


def run_preprocess():
    """
    Run app page for selected preprocessing.
    Cropping, image adjustment.
    """
    db = db_con()
    site_names = all_sites()
    select_site = st.sidebar.selectbox("Select Site", site_names)
    site_id = name_to_id(select_site)
    _, img = get_data(site_id, "Clear")

    st.subheader("Area of Interest Selection")
    st.markdown("")
    st.markdown(
        """
    - Set Area of Interest (AOI) on clear, reference image.
    - Future video feeds will use the same AOI.
    """
    )
    st.markdown("")

    c1_1, c1_2 = st.columns((2, 1))
    if select_site:
        with c1_1:
            img = Image.fromarray(img)
            cropped_img = st_cropper(img, aspect_ratio=None, return_type="box")
            pil_ratio = img.size[0] / 700
            right = cropped_img["left"] + cropped_img["width"]
            bottom = cropped_img["top"] + cropped_img["height"]

            left_scaled = round(cropped_img["left"] * pil_ratio)
            top_scaled = round(cropped_img["top"] * pil_ratio)
            right_scaled = round(right * pil_ratio)
            bottom_scaled = round(bottom * pil_ratio)
        with c1_2:
            x_coords = left_scaled, right_scaled
            y_coords = top_scaled, bottom_scaled

            st.markdown(
                f"""
            Site Name: `{select_site}` \n
            Site ID: `{site_id}`
            """
            )
            st.subheader("")
            st.success(
                f"""
            Current AOI Coordinates \n
            X-coord: `{x_coords}` \n
            Y-coord: `{y_coords}`
            """
            )
            button_store = st.button("Store Coordinates")
            if button_store:
                query = {"site_id": site_id}
                update = {"$set": {"x_coords": x_coords, "y_coords": y_coords}}
                db["kk_cv_images"].update(query, update, multi=True)

    st.subheader("Adjustment of Image Alignment")
    st.markdown("")
    st.markdown(
        """
    - Frames may be subtly offset when compared to the reference frame due to undesired camera movement.
    - Future video feeds can automatically adjust the AOI to account for this.
    """
    )
    st.markdown("")

    _, c2_2 = st.columns(2)
    with c2_2:
        toggle_offset = st.checkbox("Turn on offset adjustment", False)
    c3_1, c3_2 = st.columns(2)
    with c3_1:
        fig1 = plot_cropped(site_id)
        st.pyplot(fig1)
    with c3_2:
        fig2, offset = plot_convolution(site_id, toggle_offset)
        st.pyplot(fig2)
        st.markdown(
            f"""
        Current pixel offset: `{offset}`
        """
        )

    with st.expander("Stored AOI/Offset Values"):
        query = {"site_state": "Blocked"}
        project = {
            "_id": 0,
            "site_name": 1,
            "site_id": 1,
            "x_coords": 1,
            "y_coords": 1,
            "offset_yx": 1,
        }
        stored_values = pd.DataFrame(db["kk_cv_images"].find(query, project))
        cols = ["x_coords", "y_coords", "offset_yx"]
        for col in cols:
            stored_values[col] = [",".join(map(str, l)) for l in stored_values[col]]
        st.dataframe(stored_values)
