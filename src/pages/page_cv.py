import streamlit as st

from model.model import calculate_ssim
from model.utils import name_to_id


def run_cv():
    """Run app page for computer vision analysis."""
    with st.sidebar:
        list_site = ["Feltham", "Kinson", "Vernslade", "Westway"]
        select_site = st.selectbox("Select Site", list_site)

        list_state = ["Clear", "Blocked"]
        select_im1 = st.selectbox("Select Image 1", list_state)
        select_im2 = st.selectbox("Select Image 2", list_state, index=1)

        list_method = ["Original", "Canny", "Chan-Vese"]
        select_method = st.selectbox("Select Method", list_method)

    c1_1, _, _ = st.columns(3)

    site_id = name_to_id(select_site)

    score1, fig1, _ = calculate_ssim(
        select_site,
        select_im1,
        select_im2,
        select_method,
    )

    with st.expander("Image Differences", expanded=True):
        st.pyplot(fig1)

    with c1_1:
        st.success(
            f"""
            Similarity Score: `{round(score1, 2)}`
            """
        )
        st.markdown(
            f"""
        Site Name: `{select_site}` \n
        Site ID: `{site_id}`
        """
        )
