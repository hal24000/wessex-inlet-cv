import base64
from typing import Dict, List, Tuple, Union

import cv2
import numpy as np
import pandas as pd
import streamlit as st

from setup.database import db_con


@st.cache(ttl=600)
def all_sites() -> List[str]:
    """Get list of all unique sites."""
    db = db_con()
    query = {}
    project = {"_id": 0, "site_name": 1}
    site_names = pd.DataFrame(db["kk_cv_images"].find(query, project))[
        "site_name"
    ].unique()
    return site_names


@st.cache(ttl=600)
def get_data(site_id: str, site_state: str) -> Tuple[Dict[str, str], np.ndarray]:
    """
    Using site ID and state, returns the data (excluding image).
    Separately in a tuple, returns the image buffer using OpenCV.
    """
    db = db_con()
    query = {"site_id": site_id, "site_state": site_state}
    project = {"_id": 0}
    data = list(db["kk_cv_images"].find(query, project))[0]

    png_original = base64.b64decode(data["image"])
    png_as_np = np.frombuffer(png_original, dtype=np.uint8)
    image_buffer = cv2.imdecode(png_as_np, flags=1)
    _ = data.pop("image")
    return data, image_buffer


@st.cache(ttl=600)
def get_offset(site_id: str) -> List[str]:
    db = db_con()
    query = {
        "site_id": site_id,
        "site_state": "Blocked",
    }
    project = {"_id": 0, "offset_yx": 1}
    offset_yx = list(db["kk_cv_images"].find(query, project))[0]["offset_yx"]
    return offset_yx


def ingest_image(site_name: str, site_id: str, site_state: str, image_loc: str):
    """
    From a local directory, stores an image for a site in MongoDB.
    """
    db = db_con()
    img = cv2.imread(image_loc)
    _, buffer = cv2.imencode(".png", img)
    png_as_text = base64.b64encode(buffer)

    query = {
        "site_name": site_name,
        "site_id": site_id,
        "site_state": site_state,
    }
    update = {"$set": {"image": png_as_text}}
    db["kk_cv_images"].update(query, update)


def ingest_new_data(site_id: str, new_col: str, new_val: Union[str, int]):
    """Update images database with a new value."""
    query = {
        "site_id": site_id,
    }
    update = {"$set": {new_col: new_val}}
    db["kk_cv_images"].update(query, update, multi=True)


@st.cache(ttl=600)
def name_to_id(site_name: str) -> str:
    """Convert site name to ID."""
    db = db_con()
    query = {"site_name": site_name}
    project = {"_id": 0, "site_id": 1}
    site_id = db["kk_cv_images"].find_one(query, project)["site_id"]
    return site_id
