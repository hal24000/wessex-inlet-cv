import streamlit as st

from pages.page_preprocess import run_preprocess
from pages.page_cv import run_cv
from pages.page_dashboard import run_dashboard
from setup.layout import setup_page


setup_page(name="Grill Computer Vision")
list_page = ["Dashboard", "1 - Selected Preprocessing", "2 - Computer Vision"]
select_page = st.sidebar.selectbox("Select Page", list_page)

if select_page == list_page[0]:
    run_dashboard()

if select_page == list_page[1]:
    run_preprocess()

if select_page == list_page[2]:
    run_cv()

with st.sidebar:
    st.header("")
    st.info(
        """
    ### :information_source: Info
    - Computer Vision for automated blockage detection.
    - Calculating difference in images to detect anomalies.
    """
    )
