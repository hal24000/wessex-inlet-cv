# Computer Vision for Inlet Blockage Detection

Ingestion, cropping, processing and computer vision for detcing water inlet/outlet blockages.

## Usage
```bash
$ cd wessex-inlet-cv/src
$ streamlit run app.py
```

## Project Organisation

    ├── chart                        <- Files required for Dimensionops build
    │
    ├── docs                         <- Documentation
    │
    ├── notebooks                    <- Jupyter notebooks
    │
    ├── src                          <- Source code for project
    │   │
    │   │── model                    <- Model
    │   │   │── model.py             <- Similarity calculation model
    │   │   │── plot.py              <- Visualization plots
    │   │   └── utils.py             <- Methods to store and collect data
    │   │
    │   │── pages                    <- Pages
    │   │   │── page_cv.py           <- Computer vision models
    │   │   │── page_dashboard.py    <- Main dashboard
    │   │   └── page_preprocess.py   <- Selected preprocessing pipeline
    │   │
    │   │── setup                    <- Setup
    │   │   │── database.py          <- Database connection
    │   │   │── favicon.ico          <- Icon
    │   │   └── layout.py            <- Layout settings
    │   │
    │   └── app.py                   <- App run file
    │   
    │── Dockerfile                   <- File to assemble a Docker image
    │
    │── environment.yml              <- Environment yml file to create conda environment
    │
    ├── README.md                    <- README for this project
    │
    └── requirements.txt             <- Requirements file for creating app environment